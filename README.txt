
-- RÉSUMÉ --

Ce plugin vous permettra de créer un bloc proposant à l'utilisateur l'inscription à l'une ou plusieurs de vos newsletters e-bourgogne (http://www.e-bourgogne.fr).

Pour une description complète du projet, visitez la page :
  http://drupal.org/project/ebourgognenewsletter
Pour soumettre des rapports de bug et des suggestions, ou suivre les changements :
  http://drupal.org/project/issues/ebourgognenewsletter

-- PRÉ-REQUIS --

Pour fonctionner, le module e-bourgogne Newsletter nécessite que le module CKEditor soit installé et activé.


-- INSTALLATION --

* L'installation se fait de manière classique, voir :
  http://drupal.org/documentation/install/modules-themes/modules-8


-- CONFIGURATION --

* Allez dans Administration » Extensions et ouvrez la page de configuration du module. Renseignez votre clé d'API e-bourgogne (fournie par votre administrateur d'organisme) et cliquez sur "Enregistrer la clé" (si votre clé est valide, vous verrez apparaître les options du module).

* Allez ensuite dans  Administration » Structure » Mise en page des blocs ; dans la zone où vous souhaitez ajouter le module d'inscription, cliquez sur "Placer bloc", puis séléctionnez le bloc "Configuration block for e-bourgogne newsletter".

* Dans le panneau de configuration et choissisez pour quelle(s) newsletter(s) vous souhaitez proposer l'inscription. Choissisez également le titre et un texte descriptif pour le bloc à afficher. Cliquez ensuite sur "Enregistrer le bloc".

* Configurez le placement et l'affichage de ce bloc comme vous le souhaitez.


-- FAQ --

Q: Où puis-je récupérer ma clé d'API ?
R: La clé d'API permettant d'accéder aux services e-bourgogne via les plugins doit vous être fournie par l'administrateur de votre entité. Elle doit ensuite être renseignée dans le panneau de configuration du plugin e-bourgogne.

Q: Comment configurer mes fichiers de destinataires pour mes newsletter ?
R: La configuration des fichiers de destinataires se fait via e-bourgogne, dans le service Administration de mon site » Newsletter » Fichiers de destinataires.

-- CONTACT --

Current maintainers :
